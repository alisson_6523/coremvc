<?php

namespace App\modules\sistema\Controllers;
use App\lib\Controller;
use App\modules\sistema\Models\sistemaModel;

class home extends Controller{
    //por padrão toda classe controller executa automaticamente o method index caso não chame outro method
    public function index(){
		//carregando variavel na view
        if(!empty($_COOKIE['d'])):
            $this->load();
        else:
            $this->onePage('login');
        endif;
    }

    public function login(){
        $db_model = new sistemaModel();
        $valor = filter_input_array(INPUT_POST,FILTER_DEFAULT);
        // echo $db_model->teste();
        $db_model->login($valor['inf']);
    }

    public function Logout(){
        Logout($_COOKIE);
    }


}
